import * as React from "react"
import Typography from "@mui/material/Typography"
import Box from "@mui/material/Box"
import Login from "../components/Login"

export default function Home() {
  return (
    <Box
      sx={{
        my: 4,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Typography variant="h4" component="h1" gutterBottom>
        MUI v5 + Next.js with TypeScript example
      </Typography>
      <Login />
    </Box>
  )
}
