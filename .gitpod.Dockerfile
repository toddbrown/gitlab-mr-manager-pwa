FROM gitpod/workspace-full

# Ohymyzsh
RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Diff so fancy and update npm
RUN npm install --global diff-so-fancy npm
