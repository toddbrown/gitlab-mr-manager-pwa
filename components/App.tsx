import { TabContext, TabList, TabPanel } from "@mui/lab"
import { Box, List, Tab, Typography } from "@mui/material"
import React from "react"
import { useMyProjectsQuery } from "../generated/graphql"
import MergeRequestListItem from "./MergeRequestListItem"

type TabState = "open" | "merged" | "closed"

export default function App() {
  const [assignedTabValue, setAssignedTabValue] =
    React.useState<TabState>("open")
  const [reviewTabValue, setReviewTabValue] = React.useState<TabState>("open")
  const { data: myProjects } = useMyProjectsQuery({ pollInterval: 30_000 })

  return (
    <>
      <Typography variant="h4">Assigned Merge Requests</Typography>
      <Box sx={{ width: "100%", typography: "body1" }}>
        <TabContext value={assignedTabValue}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList
              onChange={(event, newValue: TabState) =>
                setAssignedTabValue(newValue)
              }
            >
              <Tab label="Open" value="open" />
              <Tab label="Merged" value="merged" />
              <Tab label="Closed" value="closed" />
            </TabList>
          </Box>
          <TabPanel value="open">
            <List>
              {myProjects?.currentUser?.assignedMergeRequests?.nodes?.map(
                (assignedMergeRequest, index) =>
                  assignedMergeRequest ? (
                    <MergeRequestListItem
                      assignedMergeRequest={assignedMergeRequest}
                      isLastItem={
                        index + 1 ===
                        myProjects?.currentUser?.assignedMergeRequests?.nodes
                          ?.length
                      }
                    />
                  ) : null,
              )}
            </List>
          </TabPanel>
        </TabContext>
      </Box>
      <Typography variant="h4">Reviews Requested For You</Typography>
      <Box sx={{ width: "100%", typography: "body1" }}>
        <TabContext value={reviewTabValue}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList
              onChange={(event, newValue: TabState) =>
                setReviewTabValue(newValue)
              }
            >
              <Tab label="Open" value="open" />
              <Tab label="Merged" value="merged" />
              <Tab label="Closed" value="closed" />
            </TabList>
          </Box>
          <TabPanel value="open">
            <List>
              {myProjects?.currentUser?.reviewRequestedMergeRequests?.nodes?.map(
                (assignedMergeRequest, index) =>
                  assignedMergeRequest ? (
                    <MergeRequestListItem
                      assignedMergeRequest={assignedMergeRequest}
                      isLastItem={
                        index ===
                        myProjects?.currentUser?.assignedMergeRequests?.nodes
                          ?.length
                      }
                    />
                  ) : null,
              )}
            </List>
          </TabPanel>
        </TabContext>
      </Box>
    </>
  )
}
