const baseURL = "https://gitlab.com"

export default function gitlabAuthorizationUrl() {
  const urlParameters = new URLSearchParams()
  urlParameters.set("client_id", process.env.NEXT_PUBLIC_GITLAB_CLIENT_ID ?? "")
  urlParameters.set("redirect_uri", `${window.location.origin}/callback`)
  urlParameters.set("state", "something")
  urlParameters.set("scope", "api")
  urlParameters.set("response_type", "token")

  const authorizeURL = new URL(
    `/oauth/authorize?${urlParameters.toString()}`,
    baseURL,
  )

  return authorizeURL.toString()
}
