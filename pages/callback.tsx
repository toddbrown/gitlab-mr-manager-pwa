import { useLocalStorageValue } from "@mantine/hooks"
import { Typography } from "@mui/material"
import { useRouter } from "next/router"
import React from "react"

export default function App() {
  const router = useRouter()
  const { isReady } = router.query
  const [gitlabToken, setGitlabToken] = useLocalStorageValue({
    key: "gitlabToken",
  })
  const [gitlabTokenExpiresAt, setGitlabTokenExpiresAt] = useLocalStorageValue({
    key: "gitlabTokenExpiresAt",
  })

  React.useEffect(() => {
    if (isReady) {
      return
    }

    const callbackParameters = new URLSearchParams(
      window.location.hash.replaceAll("#", ""),
    )
    const callbackToken = callbackParameters.get(`access_token`)
    // Token expiration minus 30 seconds
    const expiresAt =
      (Number.isNaN(Number(gitlabTokenExpiresAt))
        ? Date.now()
        : Number(gitlabTokenExpiresAt)) - 30_000

    // Stored token is valid
    if (expiresAt > Date.now() && callbackToken === gitlabToken) {
      void router.push("/app")
      return
    }

    const expiresIn = Number(callbackParameters.get("expires_in"))
    if (Number.isNaN(expiresIn)) {
      throw new TypeError("expires in is not a number")
    }

    setGitlabToken(callbackToken ?? "")
    setGitlabTokenExpiresAt(String(Date.now() + expiresIn * 1000))

    void router.push("/app")
  }, [
    gitlabToken,
    gitlabTokenExpiresAt,
    isReady,
    router,
    setGitlabToken,
    setGitlabTokenExpiresAt,
  ])

  return <Typography>Loading</Typography>
}
