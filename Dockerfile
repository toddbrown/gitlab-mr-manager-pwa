FROM node:16.14.2-slim as build

WORKDIR /app

COPY . .
RUN npm run build

################################################################################
# Final container
################################################################################
FROM node:16.14.2-slim as deployment

COPY package.json package-lock.json ./
RUN npm install --production
COPY --from=build /app/.next .next

CMD [ "npx", "next", "start" ]
