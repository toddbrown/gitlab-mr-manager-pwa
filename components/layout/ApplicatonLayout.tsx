import { Container } from "@mui/material"
import React from "react"

interface ApplicationLayoutProperties {
  children: React.ReactNode
}

export default function ApplicationLayout({
  children,
}: ApplicationLayoutProperties) {
  return <Container maxWidth="lg">{children}</Container>
}
