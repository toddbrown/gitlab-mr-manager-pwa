import "../styles/globals.css"
import type { AppProps } from "next/app"
import { EmotionCache } from "@emotion/cache"
import { CacheProvider, ThemeProvider } from "@emotion/react"
import { CssBaseline, useMediaQuery } from "@mui/material"
import { createTheme } from "@mui/material/styles"
import Head from "next/head"
import { red } from "@mui/material/colors"
import { ApolloProvider } from "@apollo/client"
import React from "react"
import { useLocalStorageValue } from "@mantine/hooks"
import { useRouter } from "next/router"
import createEmotionCache from "../utils/createEmotionCache"
import apolloClient from "../utils/apolloClient"
import ApplicationLayout from "../components/layout/ApplicatonLayout"

const clientSideEmotionCache = createEmotionCache()

interface MyAppProperties extends AppProps {
  emotionCache?: EmotionCache
}

export default function MyApp({
  Component,
  pageProps,
  emotionCache = clientSideEmotionCache,
}: MyAppProperties) {
  const router = useRouter()
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)")
  const [gitlabToken] = useLocalStorageValue({
    key: "gitlabToken",
    defaultValue: "",
  })

  React.useEffect(() => {
    if (router.route === "/") {
      return
    }

    const gitlabTokenExpiration = Number(gitlabToken.split(",")[1])
    if (gitlabTokenExpiration > Date.now()) {
      // TODO: add logic for token refresh
      // eslint-disable-next-line no-console
      console.log("login req'd")
    }
  }, [gitlabToken, router.route])

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? "dark" : "light",
          primary: {
            main: "#556cd6",
          },
          secondary: {
            main: "#19857b",
          },
          error: {
            main: red.A400,
          },
        },
      }),
    [prefersDarkMode],
  )

  return (
    <CacheProvider value={emotionCache}>
      <ApolloProvider client={apolloClient}>
        <Head>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
        </Head>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <ApplicationLayout>
            <Component {...pageProps} />
          </ApplicationLayout>
        </ThemeProvider>
      </ApolloProvider>
    </CacheProvider>
  )
}
