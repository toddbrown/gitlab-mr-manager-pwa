import { Divider, Grid, Link, ListItem, Typography } from "@mui/material"
import React from "react"
import { MyProjectsQuery } from "../generated/graphql"
import dateFormat from "../utils/dateFormat"
import PipelineStatusIcon from "./PipelineStatusIcon"

type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never
interface MergeRequestListItemProperties {
  assignedMergeRequest: NonNullable<
    ArrayElement<
      NonNullable<
        NonNullable<
          NonNullable<MyProjectsQuery["currentUser"]>["assignedMergeRequests"]
        >["nodes"]
      >
    >
  >
  isLastItem?: boolean
}

export default function MergeRequestListItem({
  assignedMergeRequest,
  isLastItem,
}: MergeRequestListItemProperties): JSX.Element {
  return (
    <>
      <ListItem disablePadding>
        <div style={{ width: "100%" }}>
          <Grid container padding={1}>
            <Grid item xs={10}>
              <Link
                href={assignedMergeRequest.webUrl ?? "#"}
                underline="hover"
                target="_blank"
                color="inherit"
              >
                {assignedMergeRequest.title}
              </Link>
            </Grid>
            <Grid item xs={2} sx={{ textAlign: "right" }}>
              <PipelineStatusIcon mergeRequestId={assignedMergeRequest.id} />
            </Grid>
            <Grid item xs={10}>
              <Typography variant="body2" color="text.secondary">
                {assignedMergeRequest.project.fullPath}
              </Typography>
            </Grid>
            <Grid item xs={2} sx={{ textAlign: "right" }}>
              <Typography variant="body2" color="text.secondary">
                {`updated ${dateFormat(
                  assignedMergeRequest.updatedAt as string,
                )}`}
              </Typography>
            </Grid>
          </Grid>
        </div>
      </ListItem>
      {isLastItem ? null : <Divider />}
    </>
  )
}
