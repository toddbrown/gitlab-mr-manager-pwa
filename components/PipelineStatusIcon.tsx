import React from "react"
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined"
import CheckCircleOutlinedIcon from "@mui/icons-material/CheckCircleOutlined"
import DoDisturbOutlinedIcon from "@mui/icons-material/DoDisturbOutlined"
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined"
import PlayCircleFilledOutlinedIcon from "@mui/icons-material/PlayCircleFilledOutlined"
import HelpOutlineOutlinedIcon from "@mui/icons-material/HelpOutlineOutlined"
import { Tooltip } from "@mui/material"
import {
  PipelineStatusEnum,
  usePipelineStatusQuery,
} from "../generated/graphql"
import UnreachableError from "../utils/UnreachableError"

interface PipelineStatusIconProperties {
  mergeRequestId: string
}
export default function PipelineStatusIcon({
  mergeRequestId,
}: PipelineStatusIconProperties) {
  const [pollInterval, setPollInterval] = React.useState(10_000)

  const { data: mergeRequestData } = usePipelineStatusQuery({
    variables: { mergeRequestId },
    pollInterval,
  })

  const status = mergeRequestData?.mergeRequest?.pipelines?.nodes?.find(
    (pipeline) => pipeline?.status,
  )?.status

  React.useEffect(() => {
    switch (status) {
      case PipelineStatusEnum.Success:
      case PipelineStatusEnum.Canceled:
      case PipelineStatusEnum.Failed:
      case PipelineStatusEnum.Skipped:
        setPollInterval(60_000)
        break

      default:
        setPollInterval(10_000)
        break
    }
  }, [status])

  if (!status) {
    return null
  }

  switch (status) {
    case PipelineStatusEnum.Created:
      return <AddCircleOutlineOutlinedIcon />

    case PipelineStatusEnum.Running:
      return (
        <Tooltip title="Pipeline: running">
          <PlayCircleFilledOutlinedIcon color="info" />
        </Tooltip>
      )

    case PipelineStatusEnum.Failed:
      return (
        <Tooltip title="Pipeline: failed">
          <CancelOutlinedIcon color="error" />
        </Tooltip>
      )
    case PipelineStatusEnum.Success:
      return (
        <Tooltip title="Pipeline: passed">
          <CheckCircleOutlinedIcon color="success" />
        </Tooltip>
      )

    case PipelineStatusEnum.Canceled:
    case PipelineStatusEnum.Skipped:
      return <DoDisturbOutlinedIcon />

    // Find better icons for these cases
    case PipelineStatusEnum.Manual:
    case PipelineStatusEnum.Pending:
    case PipelineStatusEnum.Preparing:
    case PipelineStatusEnum.Scheduled:
    case PipelineStatusEnum.WaitingForResource:
      return <HelpOutlineOutlinedIcon />

    default:
      throw new UnreachableError(status)
  }
}
