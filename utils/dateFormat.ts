const relativeTimeFormat = new Intl.RelativeTimeFormat("en", {
  style: "long",
  numeric: "auto",
})

const oneMinute = 1000 * 60
const oneHour = oneMinute * 60
const oneDay = oneHour * 24

export default function dateFormat(createdAt: string) {
  const timePeriod = new Date(createdAt).getTime() - Date.now()

  if (Math.abs(timePeriod) > oneDay) {
    const days = Math.round(timePeriod / oneDay)
    return relativeTimeFormat.format(days, "day")
  }

  if (Math.abs(timePeriod) > oneHour) {
    const hours = Math.round(timePeriod / oneHour)
    return relativeTimeFormat.format(hours, "hour")
  }

  const minutes = Math.round(timePeriod / oneMinute)
  return relativeTimeFormat.format(minutes, "minute")
}
