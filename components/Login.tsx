import { Button } from "@mui/material"
import gitlabAuthorizationUrl from "../utils/gitlabAuthorizationUrl"

export default function Login() {
  return (
    <Button
      onClick={() => {
        window.location.href = gitlabAuthorizationUrl()
      }}
    >
      Login
    </Button>
  )
}
